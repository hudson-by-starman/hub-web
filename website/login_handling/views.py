# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth import login, authenticate
from django.shortcuts import render, redirect
from .forms import SignUpForm
from django.contrib.auth.models import Group
from main_app.models import Default_Rank, New_User_Register_Ability

def signup(request):
    new_user_register_ability = New_User_Register_Ability.objects.values_list('new_user_register_ability', flat=True)[0]
    if new_user_register_ability == "Enabled":
        if request.method == 'POST':
            form = SignUpForm(request.POST)
            if form.is_valid():
                default_rank = Default_Rank.objects.values_list('default_rank', flat=True)[0]
                user = form.save()
                user.refresh_from_db()  # load the profile instance created by the signal
                user.profile.theme_color = form.cleaned_data.get('theme_color')
                user.profile.accent_color = form.cleaned_data.get('accent_color')
                user.save()
                raw_password = form.cleaned_data.get('password1')
                user = authenticate(username=user.username, password=raw_password)
                login(request, user)
                my_group = Group.objects.get(name= default_rank)
                my_group.user_set.add(request.user)
                return redirect('main_base')
        else:
            form = SignUpForm()
        return render(request, 'signup.html', {'form': form})
    else:
        return render(request, 'blocked.html')
