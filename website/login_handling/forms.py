from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class SignUpForm(UserCreationForm):
    theme_color_choices=[('Light','Light'), ('Dark','Dark')]
    accent_color_choices=[('Orange','Orange'), ('Green','Green'), ('Red','Red'), ('Blue','Blue')]

    theme_color = forms.ChoiceField(choices=theme_color_choices, widget=forms.RadioSelect())
    accent_color = forms.ChoiceField(choices=accent_color_choices, widget=forms.RadioSelect())

    class Meta:
        model = User
        fields = ('username', 'theme_color', 'accent_color', 'password1', 'password2', )
