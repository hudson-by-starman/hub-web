from django.conf.urls import include, url, handler404, handler500
from django.contrib import admin
from . import views
from rest_framework import routers
router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)


urlpatterns = [
    url(r'^api/', include(router.urls)),
    url('^accounts/', include('django.contrib.auth.urls')),
    url(r'', include('main_app.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'', include('login_handling.urls')),
]

handler404 = views.error_404
handler500 = views.error_500
