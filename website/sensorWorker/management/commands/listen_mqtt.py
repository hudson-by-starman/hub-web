import paho.mqtt.subscribe as subscribe
from django.core.management import BaseCommand
from datetime import datetime, timedelta
from channels import Group
import json
from sensorWorker.models import relay_status


class Command(BaseCommand):

    def handle(self, *args, **options):
        send_time = datetime.now()
        message_awaiting_reply = False

        while True:


            msg = subscribe.simple("heat_control", hostname="127.0.0.1", auth={'username':"3CaSgsJTHK6FawfAvbFXyM685rsztJ5AESozRtV82r8c9A9pZ4bkXNSodPM3ET5K9gg8zJFTEMV5GepwGovvTWiezCAmYHsrsMJm67AbQyodsNjK2W5d2M8", 'password':"85tde2SG8X4HqjqTtMVroWihzTvYi7b7BZxmyb3L6bvKDbz2AAL9eAqRcQ7yCf96s5qXTxc27LzKJwuBaUwDMQVba7ecRhereFKwpqetjftjhokqiDV8qie"})
            message = msg.payload.decode('UTF-8')
            if (message == "Heater Active"):
                Group("users").send({'text': json.dumps({ 'heater_relay_state': "connected"})})
                status=relay_status.objects.update(status="connected")
                message_awaiting_reply = False;



            if message_awaiting_reply ==  True:
                diff = datetime.now() - send_time
                if (diff.total_seconds() > 2):
                    print ("Message not recieved")
                    Group("users").send({'text': json.dumps({ 'heater_relay_state': "disconnected"})})
                    status=relay_status.objects.update(status="disconnected")
                else:
                    Group("users").send({'text': json.dumps({ 'heater_relay_state': "connected"})})
                    status=relay_status.objects.update(status="connected")


            if (message == "off"):
                print ("Attempting to send off message to relay")
                send_time = datetime.now()
                message_awaiting_reply = True
            if (message == "on"):
                print ("Attempting to send on message to relay")
                send_time = datetime.now()
                message_awaiting_reply = True


            if (message == "Device Off"):
                print ("Successfully sent off message to relay")
                message_awaiting_reply = False

            if (message == "Device On"):
                print ("Successfully sent on message to relay")
                message_awaiting_reply = False
