from django.conf.urls import url
from rest_framework_jwt.views import obtain_jwt_token
from . import views

urlpatterns = [
    url(r'^main/$', views.main_base, name='main_base'),
    url(r'^schedule_hour_toggle/$', views.schedule_hour_toggle, name='schedule_hour_toggle'),
    url(r'heater/schedule/$', views.heat_schedule, name='heat_schedule'),
    url(r'^heater_change_state/$', views.heater_change_state, name='heater_change_state'),
    url(r'^desired_temp_change/$', views.desired_temp_change, name='desired_temp_change'),
    url(r'^permission_level_change/$', views.permission_level_change, name='permission_level_change'),
    url(r'^change_default_permission_level/$', views.change_default_permission_level, name='change_default_permission_level'),
    url(r'^toggle_register_ability/$', views.toggle_register_ability, name='toggle_register_ability'),
    url(r'^toggle_ai_state/$', views.toggle_ai_state, name='toggle_ai_state'),
    url(r'^user_delete/$', views.user_delete, name='user_delete'),
    url(r'^main/admin/$', views.admin_page, name='admin_page'),
    url(r'^main/ranks/$', views.ranks, name='ranks'),
    url(r'^main/user_settings/$', views.user_settings, name='user_settings'),
    url(r'^main/change_theme/$', views.change_theme, name='change_theme'),
    url(r'heater/graphs/$', views.heater_graphs, name='heater_graphs'),
    url(r'light/$', views.light, name='light'),
    url(r'^api-token-auth/', obtain_jwt_token),
    url(r'logs/$', views.logs, name='logs'),
    url(r'dash/$', views.dash, name='dash'),
    url(r'device_search/$', views.device_search, name='device_search'),


    ]
