# -*- coding: utf-8 -*-
from django.contrib import admin
from .models import *

admin.site.register(Heater_State)
admin.site.register(Desired_Temp)
admin.site.register(Heat_Schedule)
admin.site.register(Default_Rank)
admin.site.register(New_User_Register_Ability)
admin.site.register(AI_State)
